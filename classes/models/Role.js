const {K8} = require('@komino/k8');
const ORM = K8.require('ORM');

class Role extends ORM{
  constructor(id, options) {
    super(id, options);
    if(id)return;

    //foreignKeys


    //fields
    this.name = null;
  }
}

Role.jointTablePrefix = 'role';
Role.tableName = 'roles';

Role.fields = new Map([
["name", "String"]
]);

Role.belongsTo = new Map([

]);

Role.hasMany = [
["role_id", "User"]
];

Role.belongsToMany = [

];

module.exports = Role;
