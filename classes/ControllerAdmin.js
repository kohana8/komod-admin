/* abstract class of controller require administrator role.*/
const {K8, Controller} = require('@komino/k8');
const pluralize = require('pluralize');

const ControllerMixinLoginRequire = K8.require('controller-mixin/LoginRequire');
const ControllerMixinMime = K8.require('controller-mixin/Mime');
const ControllerMixinView = K8.require('controller-mixin/View');
const ControllerMixinORM = K8.require('controller-mixin/ORM');
const ControllerMixinORMWrite = K8.require('controller-mixin/ORMWrite');
const ControllerMixinORMEdit = K8.require('controller-mixin/ORMEdit');
const ControllerMixinMultiDomainDB = K8.require('controller-mixin/MultiDomainDB');
const ControllerMixinAdminActionLogger = K8.require('controller-mixin/AdminActionLogger');
const ControllerMixinMultipartForm = K8.require('controller-mixin/MultipartForm');
const ControllerMixinAdmin = K8.require('controller-mixin/Admin');
const ControllerMixinCRUDRedirect = K8.require('controller-mixin/CRUDRedirect');

class ControllerAdmin extends Controller{
  constructor(request){
    super(request);

    this.layout = 'layout/admin/default';
    this.templates = {
      index : 'templates/admin/index',
      read  : 'templates/admin/edit',
      create: 'templates/admin/edit',
      dialog: 'templates/admin/dialog'
    };
    this.tpl = null;

    this.id = null;

    this.addMixin(new ControllerMixinLoginRequire(this, '/admin/login'))
    this.addMixin(new ControllerMixinMime(this));
    this.addMixin(new ControllerMixinAdminActionLogger(this));
    this.addMixin(new ControllerMixinMultiDomainDB(this));
    this.addMixin(new ControllerMixinView(this));
    this.addMixin(new ControllerMixinORM(this));

    this.addMixin(new ControllerMixinMultipartForm(this));
    this.addMixin(new ControllerMixinORMEdit(this));
    this.addMixin(new ControllerMixinORMWrite(this));

    this.addMixin(new ControllerMixinAdmin(this, 'admin/'));
    this.addMixin(new ControllerMixinCRUDRedirect(this, 'admin/'))
  }

  async before(){
    await super.before();
    Object.assign(this.mixin.get('view').data, {
      model            : this.model,
      action           : this.request.params.action,
      user_full_name   : this.request.session.user_full_name,
      user_role        : this.request.session.user_role,
      checkpoint       : this.mixin.get('$_GET')['cp'],
    })
  }

  async action_index(){}

  async action_multiple_update(){}

  async action_create(){}

  async action_read() {}

  async action_update(){}

  async action_delete(){}
}

module.exports = ControllerAdmin;