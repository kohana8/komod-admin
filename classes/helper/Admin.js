const {K8} = require('@komino/k8');
const RouteList = K8.require('RouteList');

class HelperAdmin{
  static crudRoute(partialPath, controller, weight= 5) {
    RouteList.add(`/admin/${partialPath}/new`, controller, 'create', RouteList.methods.GET, weight);
    RouteList.add(`/admin/${partialPath}/new`, controller, 'update', RouteList.methods.POST, weight);
    RouteList.add(`/admin/${partialPath}/:id`, controller, 'update', RouteList.methods.POST, weight);
    RouteList.add(`/admin/${partialPath}/delete/:id`, controller, 'delete', RouteList.methods.GET, weight);

    //restful
    RouteList.add(`/admin/${partialPath}`, controller, 'index', RouteList.methods.GET, weight);
    RouteList.add(`/admin/${partialPath}`, controller, 'multiple_update', RouteList.methods.POST, weight);

    RouteList.add(`/admin/${partialPath}/:id`, controller, 'read', RouteList.methods.GET, weight);
    RouteList.add(`/admin/${partialPath}/:id`, controller, 'update', RouteList.methods.PUT, weight);
    RouteList.add(`/admin/${partialPath}/:id`, controller, 'delete', RouteList.methods.DELETE, weight);


    RouteList.add(`/admin/b/:entity-:entityID/${partialPath}/new`, controller, 'create');
    RouteList.add(`/admin/b/:entity-:entityID/${partialPath}/:id`, controller, 'read');
    RouteList.add(`/admin/b/:entity-:entityID/${partialPath}/new`, controller, 'update', RouteList.methods.POST);
    RouteList.add(`/admin/b/:entity-:entityID/${partialPath}/:id`, controller, 'update', RouteList.methods.POST);
    RouteList.add(`/admin/b/:entity-:entityID/${partialPath}/delete/:id`, controller, 'delete');
  };
}

module.exports = HelperAdmin;