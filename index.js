const {K8} = require('@komino/k8');
K8.addNodeModules(require.resolve('./'));

module.exports = {
  ControllerAdmin: require('./classes/ControllerAdmin'),
  Person: require('./classes/models/Person'),
  Role: require('./classes/models/Role'),
  User: require('./classes/models/User'),
  ControllerAuth : require('./classes/controllers/ControllerAuth'),
  ControllerAdminHome : require('./classes/controllers/admin/ControllerAdminHome')
};