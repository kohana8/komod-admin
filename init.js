const {RouteList} = require('@komino/komod-route');

RouteList.add('/admin', 'controllers/admin/ControllerAdminHome');
RouteList.add('/admin/login', 'controllers/ControllerAuth', 'login');
RouteList.add('/admin/login', 'controllers/ControllerAuth', 'auth', RouteList.methods.POST);
RouteList.add('/admin/login/fail', 'controllers/ControllerAuth', 'fail');
RouteList.add('/admin/logout', 'controllers/ControllerAuth', 'logout');